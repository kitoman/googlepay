﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using GooglePay.Models;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
namespace GooglePay.Controllers
{
    [Produces("application/json")]
    public class GooglePayController : Controller
    {
        private string Endpoint = "https://google.com/pay";
        static HttpClient client = new HttpClient();

        [HttpPost, Route("api/GooglePay")]
        public async Task<IActionResult> Post([FromBody]Request request)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(
                Endpoint, new GoogleRequest());
            response.EnsureSuccessStatusCode();

            Console.WriteLine("RESPONSE : " + response);
            Response assistantResponse = new Response();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                assistantResponse.FulfillmentText = "Okay, today I'll charge your card ending in 8-2-8-6 for $1.00";
            else
                assistantResponse.FulfillmentText = "I'm sorry. I could not find your card information.";

            // Create our contextName
            var contextName = $"{request.Session}/contexts/GooglePay";
            var parameters = new Dictionary<string, object>();
            parameters.Add("bla", "bla");
            // Create our custom context with our session parameters
            var context = new OutputContext()
            {
                Name = contextName,
                LifespanCount = 10,
                Parameters = parameters
            };

            // Make it a list of our one context to match the convention of the request/response format
            var outputContexts = new List<OutputContext>();
            // Add our own context
            outputContexts.Add(context);
            
            assistantResponse.OutputContexts = outputContexts;
            // Send the request back
            return Json(assistantResponse);
        }
    }
}
