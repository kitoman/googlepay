﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GooglePay.Models
{
    public class CardInfo
    {
        public string CardDescription;
        public string CardClass;
        public string CardDetails;
        public string CardNetwork;
        public UserAddress BillingAddress;

    }
}
