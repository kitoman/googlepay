﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GooglePay.Models
{
    public class CardRequirements
    {
        public string[] AllowedCardNetworks;
        public bool BillingAddressRequired;
        public string BillingAddressFormat;
    }
}
