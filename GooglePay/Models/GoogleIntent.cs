﻿using Newtonsoft.Json;

namespace GooglePay.Models
{
    public class GoogleIntent
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "displayName")]
        public string DisplayName { get; set; }
    }
}
