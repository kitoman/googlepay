﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GooglePay.Models
{
    public class GoogleRequest
    {
        public string MerchantId = "example";
        public TransactionInfo TransactionInfo = new TransactionInfo();
        public CardRequirements CardRequirements = new CardRequirements();
        public string[] AllowedPaymentMethods = { "CARD", "TOKENIZED_CARD" };

    }
}
