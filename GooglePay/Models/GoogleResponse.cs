﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GooglePay.Models
{
    public class GoogleResponse
    {
        public PaymentMethodToken PaymentMethodToken;
        public CardInfo CardiInfo;
        public UserAddress ShippingAddress;
        public string Email;
    }
}
