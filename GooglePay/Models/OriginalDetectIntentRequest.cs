﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GooglePay.Models
{
    public class OriginalDetectIntentRequest
    {
        public Payload payload { get; set; }
    }
}
