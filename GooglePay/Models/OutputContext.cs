﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GooglePay.Models
{
    public class OutputContext
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "lifespanCount")]
        public int LifespanCount { get; set; }

        [JsonProperty(PropertyName = "parameters")]
        public Dictionary<string, object> Parameters { get; set; }
    }
}
