﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GooglePay.Models
{
    public class Payload
    {
        [JsonProperty(PropertyName="expectUserResponse")]
        public bool ExpectUserResponse { get; set; }
    }
}
