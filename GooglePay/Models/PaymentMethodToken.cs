﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GooglePay.Models
{
    public class PaymentMethodToken
    {
        public string TokenizationType;
        public string Token;
    }
}
