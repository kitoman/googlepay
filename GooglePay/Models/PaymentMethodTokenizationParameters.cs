﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace GooglePay.Models
{
    public class PaymentMethodTokenizationParameters
    {
        public string TokenizationType = "PAYMENT_GATEWAY";
        public Dictionary<string, string> Parameters = new Dictionary<string, string>();
        public PaymentMethodTokenizationParameters()
        {
            Parameters.Add("gateway", "example");
            Parameters.Add("gatewayMerchantId", "exampleGatewayMerchantId");
        }
    }
}
