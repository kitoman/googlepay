﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GooglePay.Models
{
    public class QueryResult
    {
        [JsonProperty(PropertyName = "queryText")]
        public string QueryText { get; set; }

        [JsonProperty(PropertyName = "parameters")]
        public Dictionary<string, object> Parameters { get; set; }

        [JsonProperty(PropertyName = "allRequiredParamsPresent")]
        public bool AllRequiredParamsPresent { get; set; }

        [JsonProperty(PropertyName = "fulfillmentText")]
        public string FulfillmentText { get; set; }

        [JsonProperty(PropertyName = "outputContexts")]
        public IEnumerable<OutputContext> OutputContexts { get; set; }

        [JsonProperty(PropertyName = "intent")]
        public GoogleIntent Intent { get; set; }

    }
}
