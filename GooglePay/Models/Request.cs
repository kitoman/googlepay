﻿using Newtonsoft.Json;

namespace GooglePay.Models
{
    public class Request
    {
        [JsonProperty(PropertyName = "responseId")]
        public string ResponseId { get; set; }

        [JsonProperty(PropertyName = "queryResult")]
        public QueryResult QueryResult { get; set; }

        [JsonProperty(PropertyName = "session")]
        public string Session { get; set; }
    }
}
