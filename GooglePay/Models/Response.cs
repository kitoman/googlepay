﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GooglePay.Models
{
    public class Response
    {
        [JsonProperty(PropertyName = "fulfillmentText")]
        public string FulfillmentText { get; set; }

        [JsonProperty(PropertyName = "outputContexts")]
        public IEnumerable<OutputContext> OutputContexts { get; set; }

        [JsonProperty(PropertyName = "payload")]
        public Dictionary<string, Payload> Payload { get; set; }
    }
}
