﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GooglePay.Models
{
    public class UserAddress
    {
        public string Name;
        public string PostalCode;
        public string PhoneNumber;
        public string CompanyName;
        public string Address1;
        public string Address2;
        public string Address3;
        public string Address4;
        public string Address5;
        public string Locality;
        public string AdministrativeArea;
        public string SortingCode;
    }
}
